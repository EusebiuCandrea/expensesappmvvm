package com.example.unittestingExample

import com.example.expenceappmvvm.screens.example.ExampleRepository
import com.example.expenceappmvvm.screens.example.ExampleViewModel
import com.google.common.truth.Truth.assertThat
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.ArgumentMatchers.anyInt
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.`when`
import org.mockito.Mockito.verify
import org.mockito.Spy
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class ExampleViewModelTest {
    /**
     * Naming Convention Examples:
     *
     * MethodName_ExpectedBehavior_StateUnderTest
     * example: getPost_postShouldCached_success
     *
     * Kotlin style:
     * When_StateUnderTest_Expect_ExpectedBehavior
     * example: `when server error expect post not cached`
     */
    private lateinit var viewModel: ExampleViewModel

    @Spy
    lateinit var repositorySpy: ExampleRepository

    @Mock
    lateinit var viewModelMock: ExampleViewModel

    @Before
    fun setUp() {
        viewModel = ExampleViewModel(repositorySpy)
    }

    @After
    fun tearDown() {

    }

    // SPY
    @Test
    fun checkEven_twoPlusTwo_shouldReturnTrue() {
        assertThat(viewModel.checkEvenSumNumbers(2, 2)).isTrue()
    }

    @Test
    fun checkEven_fivePlusTwo_shouldReturnFalse() {
        assertThat(viewModel.checkEvenSumNumbers(5, 2)).isFalse()
    }

    // MOCK
    @Test
    fun insertValueToDB_anyInt_shouldBeCalledOnes() {
        val viewModelPresMock = Mockito.mock(ExampleViewModel::class.java)

        viewModelPresMock.insertValueToDB(anyInt())

        verify(viewModelPresMock, Mockito.times(1)).insertValueToDB(anyInt())
    }

    @Test
    fun insertValueToDB_anyInt_shouldBeNotCalled() {
        val viewModelPresMock = Mockito.mock(ExampleViewModel::class.java)

        verify(viewModelPresMock, Mockito.times(0)).insertValueToDB(anyInt())
    }

    @Test
    fun `when check even five plus two should return false`() {
        assertThat(viewModelMock.checkEvenSumNumbers(5, 2)).isFalse()
    }

    // STUB
    @Test
    fun `when check even two plus two should return true`() {
        val repositoryPresMock = Mockito.mock(ExampleRepository::class.java)
        val viewModelPres = ExampleViewModel(repositoryPresMock)

        `when`(repositoryPresMock.add(anyInt(), anyInt())).thenReturn(4)

        assertThat(viewModelPres.checkEvenSumNumbers(2, 2)).isTrue()
    }
}