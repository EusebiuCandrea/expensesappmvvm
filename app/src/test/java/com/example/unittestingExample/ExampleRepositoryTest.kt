package com.example.unittestingExample

import com.example.expenceappmvvm.screens.example.ExampleRepository
import com.google.common.truth.Truth
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class ExampleRepositoryTest {

    private lateinit var repositoryPres: ExampleRepository

    @Before
    fun setUp() {
        repositoryPres = ExampleRepository()
    }


    @Test
    fun add_twoPlusTwo_shouldReturnFour() {
        val expectedResult = 4

        Truth.assertThat(repositoryPres.add(2, 2)).isEqualTo(expectedResult)
    }

//    @Test
//    fun add_twoPlusFive_shouldReturnTrue() {
//        val expectedResult = 7
//
//        Truth.assertThat(repositoryPres.add(2, 5)).isEqualTo(expectedResult)
//    }
}