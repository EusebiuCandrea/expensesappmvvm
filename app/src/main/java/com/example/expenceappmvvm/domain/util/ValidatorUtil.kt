package com.example.expenceappmvvm.domain.util

import java.util.regex.Pattern

object ValidatorUtil {

    fun isValidEmail(email: String?): Boolean {
        if (email == null) return false
        return Pattern.matches("^(\\S+)@([a-z0-9-]+)(\\.)([a-z]{2,4})(\\.?)([a-z]{0,4})+$", email)
    }

    fun isValidPassword(password: String?): Boolean {
        if (password == null) return false
        val regex = Pattern.compile(
            "^" +
                    "(?=.*[0-9])" +         //at least 1 digit
                    "(?=.*[a-z])" +         //at least 1 lower case letter
                    "(?=.*[A-Z])" +         //at least 1 upper case letter
                    "(?=.*[a-zA-Z])" +      //any letter
                    "(?=\\S+$)" +           //no white spaces
                    ".{8,}" +               //at least 8 characters
                    "$"
        )
        return regex.matcher(password).matches()
    }

    fun isValidName(name: String?): Boolean {
        if (name == null) return false
        return Pattern.matches("[A-Za-z][^0-9]{3,20}+\$", name.trim())
    }
}
