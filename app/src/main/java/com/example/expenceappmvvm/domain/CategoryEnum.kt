package com.example.expenceappmvvm.domain

import com.example.expenceappmvvm.R

enum class CategoryEnum {
    INCOME {
        override fun getStringValue(): String {
            return "Income"
        }
        override fun getCategoryIcon(): Int {
            return R.drawable.ic_hand
        }
    },
    FOOD {
        override fun getStringValue(): String {
            return "Food"
        }
        override fun getCategoryIcon(): Int {
            return R.drawable.ic_groceries
        }
    },
    CAR {
        override fun getStringValue(): String {
            return "Car"
        }
        override fun getCategoryIcon(): Int {
            return R.drawable.ic_car
        }
    },
    CLOTHES {
        override fun getStringValue(): String {
            return "Clothes"
        }
        override fun getCategoryIcon(): Int {
            return R.drawable.ic_laundry
        }
    },
    SAVINGS {
        override fun getStringValue(): String {
            return "Savings"
        }
        override fun getCategoryIcon(): Int {
            return R.drawable.ic_savings
        }
    },
    HEALTH {
        override fun getStringValue(): String {
            return "Health"
        }
        override fun getCategoryIcon(): Int {
            return R.drawable.ic_doctor
        }
    },
    BEAUTY {
        override fun getStringValue(): String {
            return "Beauty"
        }
        override fun getCategoryIcon(): Int {
            return R.drawable.ic_beauty
        }
    },
    TRAVEL {
        override fun getStringValue(): String {
            return "Travel"
        }
        override fun getCategoryIcon(): Int {
            return R.drawable.ic_hand
        }
    };

    abstract fun getStringValue(): String
    abstract fun getCategoryIcon(): Int
}