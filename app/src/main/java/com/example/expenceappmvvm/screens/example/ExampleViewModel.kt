package com.example.expenceappmvvm.screens.example

class ExampleViewModel(private val repositoryPres: ExampleRepository) {

    fun checkEvenSumNumbers(a: Int, b: Int): Boolean {
        val result = repositoryPres.add(a, b)
        if (result == 0) return false

        return result % 2 == 0
    }

    fun insertValueToDB(value: Int) {
        repositoryPres.insertToDB(value)
    }

}