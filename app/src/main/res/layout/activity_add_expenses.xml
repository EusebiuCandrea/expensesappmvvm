<?xml version="1.0" encoding="utf-8"?>
<layout>

    <data>

        <import type="android.view.View" />

        <variable
            name="toolbarScreenTitle"
            type="String" />

        <variable
            name="viewModel"
            type="com.example.expenceappmvvm.screens.expenses.AddExpensesViewModel" />

        <variable
            name="converter"
            type="com.example.expenceappmvvm.domain.binding.converters.BindingConverters" />
    </data>

    <androidx.constraintlayout.widget.ConstraintLayout xmlns:android="http://schemas.android.com/apk/res/android"
        xmlns:app="http://schemas.android.com/apk/res-auto"
        xmlns:tools="http://schemas.android.com/tools"
        android:id="@+id/layout_add_action"
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        android:background="@color/white"
        android:fitsSystemWindows="true">

        <com.google.android.material.appbar.AppBarLayout
            android:id="@+id/app_bar_layout"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            app:layout_constraintTop_toTopOf="parent">

            <include
                layout="@layout/toolbar_layout"
                app:addExpensesViewModel="@{viewModel}"
                app:screenTitle="@{toolbarScreenTitle}" />
        </com.google.android.material.appbar.AppBarLayout>

        <ScrollView
            android:layout_width="match_parent"
            android:layout_height="0dp"
            android:paddingBottom="@dimen/space_50dp"
            android:scrollbars="none"
            app:layout_constraintTop_toBottomOf="@+id/app_bar_layout">

            <androidx.constraintlayout.widget.ConstraintLayout
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:paddingStart="@dimen/space_18dp"
                android:paddingEnd="@dimen/space_18dp"
                tools:context=".AddActionActivity">

                <TextView
                    android:id="@+id/date_text"
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content"
                    android:layout_marginTop="@dimen/space_22dp"
                    android:fontFamily="@font/roboto"
                    android:text="@string/date"
                    android:textColor="@color/colorPrimaryDark"
                    android:textSize="@dimen/text_16sp"
                    app:layout_constraintTop_toTopOf="parent" />

                <EditText
                    android:id="@+id/date_input"
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content"
                    android:backgroundTint="@color/colorAccent"
                    android:cursorVisible="false"
                    android:focusable="false"
                    android:inputType="none"
                    android:onClick="@{()-> viewModel.onDateTimeClick()}"
                    android:textColor="@color/colorPrimaryDark"
                    app:dateFormatterAdapter="@{viewModel.expense.expenseDate}"
                    app:errorText="@{@string/invalid_date_error}"
                    app:layout_constraintTop_toBottomOf="@+id/date_text"
                    app:showErrorText="@{viewModel.invalidDate}"
                    tools:text="2018-09-03 at 18:35" />

                <TextView
                    android:id="@+id/amount_text"
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content"
                    android:layout_marginEnd="@dimen/space_20dp"
                    android:text="@string/amount"
                    android:textColor="@color/colorPrimaryDark"
                    android:textSize="@dimen/text_16sp"
                    app:layout_constraintTop_toBottomOf="@+id/date_input" />

                <EditText
                    android:id="@+id/amount_input"
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content"
                    android:backgroundTint="@color/colorAccent"
                    android:inputType="numberDecimal"
                    android:text="@={converter.toString(viewModel.expense.amount)}"
                    android:textColor="@color/colorPrimaryDark"
                    app:amountErrorHandling="@{viewModel.invalidAmount}"
                    app:errorText="@{@string/invalid_amount_error}"
                    app:layout_constraintTop_toBottomOf="@+id/amount_text"
                    tools:text="200.00" />

                <TextView
                    android:id="@+id/category_text"
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content"
                    android:layout_marginTop="@dimen/space_25dp"
                    android:text="@string/category"
                    android:textColor="@color/colorPrimaryDark"
                    android:textSize="@dimen/text_16sp"
                    app:layout_constraintTop_toBottomOf="@+id/amount_input" />

                <androidx.recyclerview.widget.RecyclerView
                    android:id="@+id/categoryRecyclerView"
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content"
                    android:layout_marginTop="@dimen/space_10dp"
                    app:layoutManager="androidx.recyclerview.widget.GridLayoutManager"
                    app:layout_constraintTop_toBottomOf="@id/category_text"
                    app:spanCount="4"
                    tools:itemCount="8"
                    tools:listitem="@layout/item_category" />

                <TextView
                    android:id="@+id/details_text"
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content"
                    android:layout_marginTop="@dimen/space_10dp"
                    android:text="@string/details"
                    android:textColor="@color/colorPrimaryDark"
                    android:textSize="@dimen/text_16sp"
                    app:layout_constraintTop_toBottomOf="@+id/categoryRecyclerView" />

                <EditText
                    android:id="@+id/details_input"
                    android:layout_width="0dp"
                    android:layout_height="wrap_content"
                    android:layout_marginEnd="@dimen/space_10dp"
                    android:backgroundTint="@color/colorAccent"
                    android:hint="@string/more_info"
                    android:text="@={viewModel.expense.details}"
                    android:textColor="@color/colorPrimaryDark"
                    app:layout_constraintEnd_toStartOf="@+id/camera_icon"
                    app:layout_constraintHorizontal_bias="1.0"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintTop_toBottomOf="@+id/details_text"
                    tools:text="More details can be added here" />

                <ImageView
                    android:id="@+id/camera_icon"
                    android:layout_width="@dimen/space_36dp"
                    android:layout_height="@dimen/space_36dp"
                    android:layout_marginEnd="@dimen/space_15dp"
                    android:onClick="@{()->viewModel.onPictureIconPress()}"
                    android:src="@drawable/ic_camera"
                    app:layout_constraintBottom_toBottomOf="@+id/details_input"
                    app:layout_constraintEnd_toEndOf="parent"
                    app:layout_constraintTop_toTopOf="@+id/details_input" />

                <ImageView
                    android:id="@+id/photo_image_view"
                    android:layout_width="208dp"
                    android:layout_height="208dp"
                    android:scaleType="centerCrop"
                    android:src="@drawable/ic_plus"
                    app:imagePath="@{viewModel.expense.picturePath}"
                    app:layout_constraintEnd_toEndOf="parent"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintTop_toBottomOf="@id/details_input" />

                <ImageView
                    android:id="@+id/remove_photo"
                    android:layout_width="wrap_content"
                    android:layout_height="wrap_content"
                    android:layout_marginStart="@dimen/space_35dp"
                    android:src="@drawable/ic_trash_can"
                    android:visibility="@{viewModel.expense.showRemovePicture ?  View.VISIBLE : View.GONE}"
                    app:layout_constraintBottom_toBottomOf="parent"
                    app:layout_constraintEnd_toEndOf="parent"
                    app:layout_constraintStart_toEndOf="@id/photo_image_view"
                    app:layout_constraintTop_toBottomOf="@id/camera_icon"
                    tools:visibility="visible" />
            </androidx.constraintlayout.widget.ConstraintLayout>
        </ScrollView>
    </androidx.constraintlayout.widget.ConstraintLayout>
</layout>